############################# Makefile ##########################
DIR_OBJ=./obj/
DIR_SRC=./src/

# Suporte
SUP_OBJ=$(DIR_OBJ)suporte.o
SUP_SRC=$(DIR_SRC)suporte.c

# Pthreads
THREAD_EXE=multiplicador_pthreads
THREAD_OBJ=$(DIR_OBJ)$(THREAD_EXE).o
THREAD_SRC=$(DIR_SRC)$(THREAD_EXE).c

# Processos
PROC_EXE=multiplicador_process
PROC_OBJ=$(DIR_OBJ)$(PROC_EXE).o
PROC_SRC=$(DIR_SRC)$(PROC_EXE).c

# Gerador de matrizes
GERADOR_EXE=gera_matriz
GERADOR_OBJ=$(DIR_OBJ)$(GERADOR_EXE).o
GERADOR_SRC=$(DIR_SRC)$(GERADOR_EXE).c


all: make_suporte_obj pthread processos gerador

# Gera o obj da biblioteca suporte
make_suporte_obj: 
	gcc -o $(SUP_OBJ) -c $(SUP_SRC)


# Gera o multiplicador utilizando pthreads
pthread: 
	gcc -o $(THREAD_OBJ) -c $(THREAD_SRC)
	gcc -o $(THREAD_EXE) $(SUP_OBJ) $(THREAD_OBJ) -lpthread


# Gera o multiplicador utilizando processos
processos: 
	gcc -o $(PROC_OBJ) -c $(PROC_SRC)
	gcc -o $(PROC_EXE) $(SUP_OBJ) $(PROC_OBJ)


# Gera um gerador de matrizes
gerador: 
	gcc -o $(GERADOR_OBJ) -c $(GERADOR_SRC)
	gcc -o $(GERADOR_EXE) $(SUP_OBJ) $(GERADOR_OBJ)


clean:
	rm -rf $(DIR_OBJ)*.o
	rm -rf $(THREAD_EXE)
	rm -rf $(PROC_EXE)
	rm -rf $(GERADOR_EXE)

