#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/suporte.h"

#define ARQ_M1 "in1.txt"
#define ARQ_M2 "in2.txt"

#define FAIXA_PADRAO 1000


/**
* Programa que gera duas matrizes aleatorias de tamanhos, respectivamente,
* n1 x m e m x n2 (passados como parâmetros) e são gravadas nos arquivos
* in1.txt in2.txt.
* Parametros na chamda do programa:
*
*    nome_do_programa -m1 <lin_m1> <col_m1> -m2 <lin_m2> <col_m2> -f <faixa> -n
*
* -m1 <lin_m1> <col_m1>: informações dos números de linhas e as colunas da matriz 1.
* -m2 <lin_m2> <col_m2>: informações dos números de linhas e as colunas da matriz 2.
* -f <faixa> (opcional): valor máximo, não incluso, dos elementos das matrizes.
*    Quando -n estiver marcado, este será o valor mínimo também. Valor padrão: faixa = 1000.
* -n (opciona): indica se as matrizes deverão conter números negativos.
*/
int main(int argc, char* argv[])
{
    int i = 0;
    int lin_m1 = 0, col_m1 = 0;
    int lin_m2 = 0, col_m2 = 0;
    int faixa = FAIXA_PADRAO;
    int negativos = 0;

    i = 0;

    while(i < argc)
    {
        if(strcmp(argv[i], "-m1") == 0)
        {
            i++;
            lin_m1 = atoi(argv[i]);
            i++;
            col_m1 = atoi(argv[i]);
        }

        if(strcmp(argv[i], "-m2") == 0)
        {
            i++;
            lin_m2 = atoi(argv[i]);
            i++;
            col_m2 = atoi(argv[i]);
        }

        if(strcmp(argv[i], "-f") == 0)
        {
            i++;
            faixa = atoi(argv[i]);
        }

        if(strcmp(argv[i], "-n") == 0)
        {
            negativos = 1;
        }

        i++;
    }

    int avancar = 1;
    if(lin_m1 <= 0)
    {
        printf("Quantidades de linhas da Matriz 1 deve ser maior do que zero!\n");
        avancar = 0;
    }
    if(col_m1 <= 0)
    {
        printf("Quantidades de colunas da Matriz 1 deve ser maior do que zero!\n");
        avancar = 0;
    }
    if(lin_m2 <= 0)
    {
        printf("Quantidades de linhas da Matriz 2 deve ser maior do que zero!\n");
        avancar = 0;
    }
    if(col_m2 <= 0)
    {
        printf("Quantidades de colunas da Matriz 2 deve ser maior do que zero!\n");
        avancar = 0;
    }

    if(faixa <= 0)
    {
        printf("A faixa de valores deve ser maior do que zero!\n");
        avancar = 0;
    }
    if(faixa > RAND_MAX)
    {
        printf("A faixa nao pode ser maior do que %d (RAND_MAX)!\n", RAND_MAX);
        avancar = 0;
    }

    if(!avancar)
    {
        printf("\nParametros invalidos!\n");
        return 0;
    }

    if(col_m1 != lin_m2)
    {
        printf("Aviso! O numero de colunas da matriz 1 eh diferente do que a da matriz 2!\n");
    }

    int **matriz1 = malloc(sizeof(int)*lin_m1*col_m1);
    int **matriz2 = malloc(sizeof(int)*lin_m2*col_m2);

    sup_gera_matriz_aleatoria(lin_m1, col_m1, (int (*)[(col_m1)])matriz1, faixa, negativos);
    sup_gera_matriz_aleatoria(lin_m2, col_m2, (int (*)[(col_m2)])matriz2, faixa, negativos);

    if(!sup_grava_matriz(ARQ_M1, lin_m1, col_m1, (int (*)[(col_m1)])matriz1))
    {
        printf("Erro ao gravar a matriz 1!\n");
        return 0;
    }

    if(!sup_grava_matriz(ARQ_M2, lin_m2, col_m2, (int (*)[(col_m2)])matriz2))
    {
        printf("Erro ao gravar a matriz 2!\n");
        return 0;
    }

    printf("Arquivos gerados com sucesso!\n");

    return 0;
}

