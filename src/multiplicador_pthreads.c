#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include "../include/suporte.h"


void *multiplicador_matriz(void *args)
{
    int **param = (int**)args;
    sup_multiplica_matriz(*param[0], *param[1], (int (*)[(*param[0])])param[2], *param[3], *param[4]
        , (int (*)[(*param[4])])param[5], (int (*)[(*param[4])])param[6], *param[7], *param[8]);

    pthread_exit(NULL);
}


int main(int argc, char *argv[]) {
    int num_threads = 0;

    // Extrai o número de threads passados como parametro
    if(1 < argc)
        num_threads = sup_extrai_numeros(argv[1]);

    if(num_threads < 1)
    {
        printf("O numero de threads deve ser maior do que 0");
        return 0;
    }

    FILE *arq_m1, *arq_m2;
    arq_m1 = fopen(ARQ_M1, "r+");
    arq_m2 = fopen(ARQ_M2, "r+");

    int lin_m1, lin_m2;
    int col_m1, col_m2;
    char linhas[80];
    char colunas[80];

    // Busca o número de linhas e colunas das matrizes m1 e m2
    fgets(linhas, 80, arq_m1);
    fgets(colunas, 80, arq_m1);
    lin_m1 = sup_extrai_numeros(linhas);
    col_m1 = sup_extrai_numeros(colunas);

    fgets(linhas, 80, arq_m2);
    fgets(colunas, 80, arq_m2);
    lin_m2 = sup_extrai_numeros(linhas);
    col_m2 = sup_extrai_numeros(colunas);

    if(col_m1 != lin_m2)
    {
        printf("O numero de colunas da matriz 1 deve ser igual ao numero de linhas da matriz 2!\n");
        fclose(arq_m1);
        fclose(arq_m2);
        return 0;
    }

    int **m1 = malloc(sizeof(int)*lin_m1*col_m1);
    int **m2 = malloc(sizeof(int)*lin_m2*col_m2);

    // Carrega as matrizes
    sup_carrega_matriz(lin_m1, col_m1, (int (*)[(col_m1)]) m1, arq_m1);
    fclose(arq_m1);

    sup_carrega_matriz(lin_m2, col_m2, (int (*)[(col_m2)]) m2, arq_m2);
    fclose(arq_m2);

    // Zera a matriz resultante mr
    int **mr = malloc(sizeof(int)*lin_m1*col_m2);
    sup_zera_matriz(lin_m1, col_m2, (int (*)[(col_m2)]) mr);


    // Inicia as threads e os argumentos da função multiplicador_matriz
    pthread_t threads[num_threads];
    int *args[num_threads][9];

    int i;
    for(i = 0; i < num_threads; i++)
    {
        args[i][0] = &lin_m1;
        args[i][1] = &col_m1;
        args[i][2] = (int (*)) m1;
        args[i][3] = &lin_m2;
        args[i][4] = &col_m2;
        args[i][5] = (int (*)) m2;
        args[i][6] = (int (*)) mr;
        args[i][7] = malloc(sizeof(int));
        *args[i][7] = i;
        args[i][8] = &num_threads;
        pthread_create(&threads[i], NULL, multiplicador_matriz, (void *)args[i]);
    }

    for(i = 0; i < num_threads; i++)
    {
        pthread_join(threads[i], NULL);
        // Desaloca memória do id
        free(args[i][7]);
    }



    if(sup_grava_matriz(ARQ_SAIDA, lin_m1, col_m2, (int (*)[(col_m2)]) mr))
        printf("\nArquivo gerado com sucesso em %s.\n", ARQ_SAIDA);

    // Desaloca memória das matrizes
    free(m1);
    free(m2);
    free(mr);

    return 0;
}
