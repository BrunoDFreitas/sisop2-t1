#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include "../include/suporte.h"


int main(int argc, char *argv[]) {
    int num_processos = 0;

    // Extrai o número de threads passados como parametro
    if(1 < argc)
        num_processos = sup_extrai_numeros(argv[1]);

    if(num_processos < 1)
    {
        printf("O numero de processos deve ser maior do que 0");
        return 0;
    }

    FILE *arq_m1, *arq_m2;
    arq_m1 = fopen(ARQ_M1, "r+");
    arq_m2 = fopen(ARQ_M2, "r+");

    int lin_m1, lin_m2;
    int col_m1, col_m2;
    char linhas[80];
    char colunas[80];

    // Busca o número de linhas e colunas das matrizes m1 e m2
    fgets(linhas, 80, arq_m1);
    fgets(colunas, 80, arq_m1);
    lin_m1 = sup_extrai_numeros(linhas);
    col_m1 = sup_extrai_numeros(colunas);

    fgets(linhas, 80, arq_m2);
    fgets(colunas, 80, arq_m2);
    lin_m2 = sup_extrai_numeros(linhas);
    col_m2 = sup_extrai_numeros(colunas);

    if(col_m1 != lin_m2)
    {
        printf("O numero de colunas da matriz 1 deve ser igual ao numero de linhas da matriz 2!\n");
        fclose(arq_m1);
        fclose(arq_m2);
        return 0;
    }

    int **m1 = malloc(sizeof(int)*lin_m1*col_m1);
    int **m2 = malloc(sizeof(int)*lin_m2*col_m2);

    // Carrega as matrizes
    sup_carrega_matriz(lin_m1, col_m1, (int (*)[(col_m1)]) m1, arq_m1);
    fclose(arq_m1);

    sup_carrega_matriz(lin_m2, col_m2, (int (*)[(col_m2)]) m2, arq_m2);
    fclose(arq_m2);

    // Cria um espaço de memória compartilhada
    int mem_comp = shmget(IPC_PRIVATE, sizeof(int)*lin_m1*col_m2, IPC_CREAT|0666);

    // Zera a matriz resultante mr
    int **mr = (int**)shmat(mem_comp, NULL, 0);
    sup_zera_matriz(lin_m1, col_m2, (int (*)[(col_m2)]) mr);


    // Identificador dos processos
    int id = 0;

    // Cria os processos filhos que serão responsáveis pela multiplicação
    int i;
    for(i = 0; i < num_processos; i++)
    {
        pid_t pid = fork();

        // Se o processo for filho, então multiplicara algumas linhas da matriz resultate
        // Senão o processo pai continuara a criar novos processos
        if(pid == 0)
        {
            sup_multiplica_matriz(lin_m1, col_m1, (int (*)[(col_m1)]) m1
                                    , lin_m2, col_m2, (int (*)[(col_m2)]) m2
                                    , (int (*)[(col_m2)]) mr, id, num_processos);
            return 0;
        }
        id++;
    }

    // Aguarda os processos filhos terminarem de executar
    for(i = 0; i < num_processos; i++)
    {
        waitpid(0, NULL, 0);
    }

    // Grava a matriz resultante no arquivo de saída
    if(sup_grava_matriz(ARQ_SAIDA, lin_m1, col_m2, (int (*)[(col_m2)]) mr))
        printf("\nArquivo gerado com sucesso em %s.\n", ARQ_SAIDA);

    // Libera a memória compartilhada
    shmctl(mem_comp, IPC_RMID, NULL);

    // Desaloca memória das matrizes
    free(m1);
    free(m2);
//    free(mr); // Não é necessário desalocar a memória, pois ja foi desalocada no comando shmctl

    return 0;
}
