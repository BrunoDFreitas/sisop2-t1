#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/types.h> /* for pid_t */
#include <sys/wait.h> /* for wait */
#include "../include/suporte.h"

#define TAM 1000

int main(int argc, char *argv[]) {
    FILE *m1, *m2;
    m1 = fopen(ARQ_M1, "r+");
    m2 = fopen(ARQ_M2, "r+");

    int i, j, k, somaprod;
    int linha1, linha2;
    int coluna1, coluna2;
    char linhas[80];
    char colunas[80];

    fgets(linhas, 80, m1);
    fgets(colunas, 80, m1);
    linha1 = sup_extrai_numeros(linhas);
    coluna1 = sup_extrai_numeros(colunas);

    fgets(linhas, 80, m2);
    fgets(colunas, 80, m2);
    linha2 = sup_extrai_numeros(linhas);
    coluna2 = sup_extrai_numeros(colunas);

    if(coluna1 != linha2)
    {
        printf("O numero de colunas da matriz 1 deve ser igual ao numero de linhas da matriz 2!\n");
        fclose(m1);
        fclose(m2);
        return 0;
    }


    int matriz1[linha1][coluna1];
    int matriz2[linha2][coluna2];

    sup_carrega_matriz(linha1, coluna1, matriz1, m1);
    fclose(m1);

    sup_carrega_matriz(linha2, coluna2, matriz2, m2);
    fclose(m2);

    int matriz3[linha1][coluna2];
    for (i = 0; i < linha1; i++) {
        for (j = 0; j < coluna2; j++) {
            matriz3[i][j] = 0;
        }
    }

//    if (coluna1 == linha2) {
    for (i = 0; i < linha1; i++) {
        for (j = 0; j < coluna2; j++) {
            for (k = 0; k < linha2; k++) {
                matriz3[i][j] = matriz3[i][j] + (matriz1[i][k] * matriz2[k][j]);
            }
        }
    }
    sup_grava_matriz(ARQ_SAIDA, linha1, coluna2, matriz3);
//    } else {
//        printf("\nErro! Impossivel multiplicar as matrizes informadas.\n");
//
//    }
}
