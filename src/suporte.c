#include "../include/suporte.h"
//#include "suporte.h"

#include <ctype.h>
#include <stdlib.h>

/** Função usada para carregar a matriz de um arquivo */
int sup_carrega_matriz(int linhas, int colunas, int matriz[linhas][colunas], FILE* pFile)
{
    int i, j;
        for(i = 0; i < linhas; i++)
        {
            for(j = 0; j < colunas; j++)
            {
                if(!fscanf(pFile, "%d", &matriz[i][j]))
                {
                    printf("\nErro ao ler a entrada (%d,%d) da matriz!\n", i, j);
                    return 0;
                }
            }
        }
    return 1;
}

/** Imprime no terminal a matriz */
void sup_imprime_matriz (int linhas, int colunas, int matriz[linhas][colunas])
{
    int i, j;
    for(i = 0; i < linhas; i++)
    {
        for(j = 0; j < colunas; j++)
        {
            printf("%d ", matriz[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

/** Grava a matriz no arquivo arq_saida */
int sup_grava_matriz (char* arq_saida, int linhas, int colunas, int matriz[linhas][colunas])
{
    FILE *fp;
    fp = fopen(arq_saida,"w+");
    if(fp == NULL)
    {
        printf("Erro ao criar o arquivo de saida %s.\n", arq_saida);
        return 0;
    }

    fprintf(fp, "LINHAS = %d\n", linhas);
    fprintf(fp, "COLUNAS = %d\n", colunas);

    int i, j;
    for(i = 0; i < linhas; i++)
    {
        for(j = 0; j < colunas; j++)
        {
            fprintf(fp,"%d ", matriz[i][j]);
        }
        fprintf(fp,"\n");
    }
    fclose(fp);
    return 1;
}


/** Extrai matriz de uma string */
int sup_extrai_numeros(char * string)
{
    int chars=0; //variável onde ficam armazenados os dígitos extraídos
    int i;

    for(i=0; string[i]!='\0'; i++) //percorrer todos os caracteres
    {
        if(isdigit(string[i])) //verificar se o carácter é numérico
        {
            chars += (int)(string[i])-'0'; //forma "artesanal" de converter o carácter para inteiro
            chars *= 10;
        }
    }
    chars /= 10;

    return chars;
}


/** Gera uma matriz aleatória de tamanho 'linhas' x 'colunas' com valores de até no máximo 'faixa' (não incluso) e se devem ou não serem negativos*/
void sup_gera_matriz_aleatoria(int linhas, int colunas, int matriz[linhas][colunas], int faixa, int negativos)
{
    int i, j, n;
    for(i = 0; i < linhas; i++)
    {
        for(j = 0; j < colunas; j++)
        {
            n = rand() % faixa;

            if(negativos)
                if(rand() % 2 == 1)
                    n *= -1;

            matriz[i][j] = n;
        }
    }
}


/** Zera todos os elementos da matriz */
void sup_zera_matriz(int linhas, int colunas, int matriz[linhas][colunas])
{
    int i, j;
    for (i = 0; i < linhas; i++) {
        for (j = 0; j < colunas; j++) {
            matriz[i][j] = 0;
        }
    }
}

/** Extrai a coluna num_col da matriz */
void sup_extrai_coluna_matriz(int linhas, int colunas, int matriz[linhas][colunas], int num_coluna, int coluna[])
{
    int i;
    for(i = 0; i < linhas; i++)
    {
        coluna[i] = matriz[i][num_coluna];
    }
}


/** Calcula a soma das multiplicações entre uma linha e uma coluna */
int sup_calcula_elemento_matriz(int num_col_m1, int linha_m1[], int coluna_m2[])
{
    int i, resultado = 0;
    for(i = 0; i < num_col_m1; i++)
    {
        resultado = resultado + (linha_m1[i]*coluna_m2[i]);
    }

    return resultado;
}


/** Cada thread/processo calculará lin_m1/num_threads linhas da matriz resultante
*** onde as linhas l escolhidas serão as l = id + num_threads * n, sendo n = 0, 1, 2...
*** sendo l < lin_m1.
***/
void sup_multiplica_matriz(int lin_m1, int col_m1, int m1[lin_m1][col_m1],
                        int lin_m2, int col_m2, int m2[lin_m2][col_m2],
                        int mr[lin_m1][col_m2], int id, int num_threads)
{
    int coluna_m2[lin_m2];

    int i, j;
    
/*
    for(i = id; i < lin_m1; i += num_threads)
    {
        for(j = 0; j < col_m2; j++)
        {
            sup_extrai_coluna_matriz(lin_m2, col_m2, m2, j, coluna_m2);

            mr[i][j] = sup_calcula_elemento_matriz(col_m1, (int *) &m1[i], coluna_m2);
        }
    }
*/  
    // Invertido a ordem por questão de desempenho, desta maneira não é necessário extrair a coluna de m2
    // para cada vez que é calculado um elemento. Dessa, dessa maneira será calculado o primeiro elemento
    // de cada linha para depois calcular o segundo elemento de cada linha e assim por diante.
    for(j = 0; j < col_m2; j++)
    {
        sup_extrai_coluna_matriz(lin_m2, col_m2, m2, j, coluna_m2);
        
        for(i = id; i < lin_m1; i += num_threads)
        {
            mr[i][j] = sup_calcula_elemento_matriz(col_m1, (int *) &m1[i], coluna_m2);
        }
    }
}
