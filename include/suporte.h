#ifndef SUPORTE_H
#define SUPORTE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>

#define ARQ_M1 "in1.txt"
#define ARQ_M2 "in2.txt"
#define ARQ_SAIDA "out.txt"

/** Função usada para carregar a matriz de um arquivo */
int sup_carrega_matriz(int linhas, int colunas, int matriz[linhas][colunas], FILE* pFile);

/** Imprime no terminal a matriz */
void sup_imprime_matriz (int linhas, int colunas, int matriz[linhas][colunas]);

/** Grava a matriz no arquivo arq_saida */
int sup_grava_matriz (char* arq_saida, int linhas, int colunas, int matriz[linhas][colunas]);

/** Extrai matriz de uma string */
int sup_extrai_numeros(char * string);

/** Gera uma matriz aleatória de tamanho 'linhas' x 'colunas' com valores de até no máximo 'faixa' (não incluso) e se devem ou não serem negativos*/
void sup_gera_matriz_aleatoria(int linhas, int colunas, int matriz[linhas][colunas], int faixa, int negativos);

/** Zera todos os elementos da matriz */
void sup_zera_matriz(int linhas, int colunas, int matriz[linhas][colunas]);

/** Extrai a coluna num_col da matriz */
void sup_extrai_coluna_matriz(int linhas, int colunas, int matriz[linhas][colunas], int num_coluna, int coluna[linhas]);

/** Calcula a soma das multiplicações entre uma linha e uma coluna */
int sup_calcula_elemento_matriz(int num_col_m1, int linha_m1[], int coluna_m2[]);

/** Cada thread/processo calculará lin_m1/num_threads linhas da matriz resultante
*** onde as linhas l escolhidas serão as l = id + num_threads * n, sendo n = 0, 1, 2...
*** sendo l < lin_m1.
***/
void sup_multiplica_matriz(int lin_m1, int col_m1, int m1[lin_m1][col_m1], int lin_m2, int col_m2, int m2[lin_m2][col_m2], int mr[lin_m1][col_m2], int id, int num_threads);

#ifdef __cplusplus
}
#endif

#endif /* SUPORTE_H */

